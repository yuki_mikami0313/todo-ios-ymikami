//
//  TodosGetRequestTests.swift
//  TodoAppTests
//
//  Created by ymikami on 2020/06/26.
//  Copyright © 2020 ymikami. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodosGetRequestTests: XCTestCase {

    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodosGetRequest()

        XCTAssertEqual(request.method, .get)
        XCTAssertEqual(request.path, "/todos")
    }

    func testResponse() {
        var todosGetReponse: TodosGetResponse?

        stub(condition: isHost("todo-api-ymikami.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("GetResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Get Todo Request")
        
        APIClient().call(
            request: TodosGetRequest(),
            success: { response in
                todosGetReponse = response
                expectation.fulfill()
        },
            failure: { _ in
                return
        })

        // 成功時のレスポンステスト
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todosGetReponse)
            XCTAssertEqual(todosGetReponse?.todos.count, 3)
            XCTAssertEqual(todosGetReponse?.todos[0].title, "test")
            XCTAssertEqual(todosGetReponse?.todos[0].date, "2020-06-22T00:00:00.000Z".toDate())
            XCTAssertEqual(todosGetReponse?.todos[1].detail, "あいうえお")
            XCTAssertNil(todosGetReponse?.todos[2].detail)
            XCTAssertNil(todosGetReponse?.todos[2].date)
            XCTAssertEqual(todosGetReponse?.errorCode, 0)
            XCTAssertEqual(todosGetReponse?.errorMessage, "")
        }
    }
}

extension String {
    func toDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        return dateFormatter.date(from: self)
    }
}
