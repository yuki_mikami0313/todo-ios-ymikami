//
//  TodosGetRequest.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/12.
//  Copyright © 2020 ymikami. All rights reserved.
//

import Alamofire

struct TodosGetRequest: RequestProtocol {
    typealias Response = TodosGetResponse

    var parameters: Parameters? {
        return nil
    }
    
    var path: String {
        return "/todos"
    }
    
    var method: HTTPMethod {
        return .get
    }
}
