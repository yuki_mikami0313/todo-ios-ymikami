//
//  TodoCreateRequest.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/18.
//  Copyright © 2020 ymikami. All rights reserved.
//

import Foundation
import Alamofire

struct TodoCreateRequest: RequestProtocol {
    typealias Response = CommonResponse

    let title: String
    let detail: String?
    let date: Date?
    
    var path: String {
        return "/todos"
    }

    var method: HTTPMethod {
        return .post
    }

    var parameters: Parameters? {
        var parameters = ["title": title]
        if let detail = detail {
            parameters["detail"] = detail
        }
        if let date = date {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            parameters["date"] = formatter.string(from: date)
        }
        return parameters
    }
}
