//
//  TodoDeleteRequest.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/24.
//  Copyright © 2020 ymikami. All rights reserved.
//

import Alamofire

struct TodoDeleteRequest: RequestProtocol {
    typealias Response = CommonResponse

    let id: Int

    var parameters: Parameters? {
        return nil
    }

    var path: String {
        return "/todos/\(id)"
    }

    var method: HTTPMethod {
        return .delete
    }
}
