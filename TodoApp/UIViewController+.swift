//
//  UIViewController+.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/18.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit

extension UIViewController {
    func showErrorAlert(message: String) {
        let alertController = UIAlertController(title: "エラー",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "閉じる",
                                                style: .default))
        present(alertController, animated: true)
    }
}
