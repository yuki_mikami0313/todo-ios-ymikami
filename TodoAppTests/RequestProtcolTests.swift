//
//  RequestProtcolTests.swift
//  TodoAppTests
//
//  Created by ymikami on 2020/06/26.
//  Copyright © 2020 ymikami. All rights reserved.
//

import XCTest
import Alamofire

final class RequestProtocolTests: XCTestCase {

    func testInit() {
        let request = TestRequestProtocol()

        XCTAssertNil(request.parameters)
        XCTAssertEqual(request.baseUrl, "https://todo-api-ymikami.herokuapp.com")
        XCTAssertEqual(request.headers?["Content-Type"], "application/json")
        XCTAssertEqual(request.encoding.toJsonEncoding(), JSONEncoding.default)
        XCTAssertEqual(request.method, .get)
        XCTAssertEqual(request.path, "/todos")
    }
}

extension ParameterEncoding {
    func toJsonEncoding() -> JSONEncoding? {
        self as? JSONEncoding
    }
}

extension JSONEncoding: Equatable {
    public static func == (lhs: JSONEncoding, rhs: JSONEncoding) -> Bool {
        return lhs.options == rhs.options
    }
}
