//
//  TodoEditViewController.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/05.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit
import Alamofire

protocol TodoEditViewControllerDelegate: class {
    func todoEditViewControllerDidTodoEdit(_ todoEditViewController: TodoEditViewController, message: String)
}

final class TodoEditViewController: UIViewController {
    weak var delegate: TodoEditViewControllerDelegate?
    @IBOutlet private weak var registrationButton: UIButton!
    @IBOutlet private weak var detailTextView: UITextView!
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var dateTextField: UITextField!
    @IBOutlet private weak var detailCountLabel: UILabel!
    @IBOutlet private weak var titleCountLabel: UILabel!

    var todo: Todo?
    private let datePicker = UIDatePicker()
    private let dateFormatter = DateFormatter()
    private let titleMaxCount = 100
    private let detailMaxCount = 1000
    private var titleText: String {
        return titleTextField.text ?? ""
    }
    private var detailText: String {
        return detailTextView.text ?? ""
    }
    private var isTitleOverLimit: Bool {
        return countTitle > titleMaxCount
    }
    private var isDetailOverLimit: Bool {
        return countDetail > detailMaxCount
    }
    private var countTitle: Int {
        return titleText.count
    }
    private var countDetail: Int {
        return detailText.count
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy/M/d"
        setUpViews()
        setUpNavigationItem()
        setUpRegistrationButton()
        setUpDatePicker()
    }

    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
    }

    private func setUpViews() {
        detailTextView.layer.borderColor = UIColor.gray.cgColor
        detailTextView.layer.borderWidth = 1.0
        detailTextView.layer.cornerRadius = 10.0
        detailTextView.layer.masksToBounds = true
        detailTextView.delegate = self
        if let todo = todo {
            setText(todo: todo)
            setUpCounter()
        }
    }

    private func setUpRegistrationButton() {
        if todo != nil {
            registrationButton.setTitle("更新", for: .normal)
        }
        updateRegistrationButton()
        registrationButton.layer.borderWidth = 0.5
        registrationButton.layer.borderColor = UIColor.black.cgColor
        registrationButton.layer.cornerRadius = 5.0
        registrationButton.setTitleColor(.white, for: .normal)
    }

    private func setUpDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = Locale.current
        dateTextField.inputView = datePicker
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 35))
        let flexibleSpaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let deleteButtonItem = UIBarButtonItem(title: "削除", style: .done, target: self, action: #selector(didDeleteButtonTapped))
        let decisionButtonItem = UIBarButtonItem(title: "決定", style: .done, target: self, action: #selector(didDecisionButtonTapped))
        let closeButtonItem = UIBarButtonItem(title: "閉じる", style: .done, target: self, action: #selector(didCloseButtonTapped))
        
        toolbar.setItems([deleteButtonItem, flexibleSpaceItem, closeButtonItem, decisionButtonItem], animated: true)
        dateTextField.inputAccessoryView = toolbar
    }

    @objc private func didDecisionButtonTapped() {
        dateTextField.endEditing(true)
        dateTextField.text = dateFormatter.string(from: datePicker.date)
    }

    @objc private func didCloseButtonTapped() {
        dateTextField.endEditing(true)
    }

    @objc private func didDeleteButtonTapped() {
        dateTextField.endEditing(true)
        dateTextField.text = ""
    }

    private func disableRegistrationButton() {
        registrationButton.isEnabled = false
        registrationButton.backgroundColor = .silver
    }

    private func enableRegistrationButton() {
        registrationButton.isEnabled = true
        registrationButton.backgroundColor = .darkblue
    }

    private func isEnableRegistrationButton() -> Bool {
        return !titleText.isEmpty && !isTitleOverLimit && !isDetailOverLimit
    }

    private func updateRegistrationButton() {
        if isEnableRegistrationButton() {
            enableRegistrationButton()
        } else {
            disableRegistrationButton()
        }
    }

    private func registerTodo(title: String, detail: String?, date: Date?) {
        APIClient().call(
            request: TodoCreateRequest(title: title, detail: detail, date: date),
            success: { [weak self] result in
                guard let `self` = self else { return }
                self.delegate?.todoEditViewControllerDidTodoEdit(self, message: "登録しました")
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func updateTodo(id: Int, title: String, detail: String?, date: Date?) {
        let todo = Todo(id: id, title: title, detail: detail, date: date)
        APIClient().call(
            request: TodoUpdateRequest(todo: todo),
            success: { [weak self] result in
                guard let `self` = self else { return }
                self.delegate?.todoEditViewControllerDidTodoEdit(self, message: "更新しました")
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func setText(todo: Todo) {
        titleTextField.text = todo.title
        detailTextView.text = todo.detail
        if let date = todo.date {
            dateTextField.text = dateFormatter.string(from: date)
        }
    }

    private func setUpCounter() {
        titleCountLabel.text = "\(countTitle)"
        detailCountLabel.text = "\(countDetail)"
    }

    @IBAction private func didTitleTextFieldTextChanged(_ sender: Any) {
        titleCountLabel.text = "\(countTitle)"

        updateRegistrationButton()

        if isTitleOverLimit {
            titleCountLabel.textColor = .red
        } else {
            titleCountLabel.textColor = .black
        }
    }

    @IBAction private func onClickRegistrationButton(_ sender: Any) {
        let dateText = dateTextField.text ?? ""
        let date = dateFormatter.date(from: dateText)
        let detail = detailText.isEmpty ? nil : detailText
        if let id = todo?.id {
            updateTodo(id: id, title: titleText, detail: detail, date: date)
        } else {
            registerTodo(title: titleText, detail: detail, date: date)
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        titleTextField.endEditing(true)
        detailTextView.endEditing(true)
    }
}

extension TodoEditViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        detailCountLabel.text = "\(countDetail)"

        updateRegistrationButton()

        if isDetailOverLimit {
            detailCountLabel.textColor = .red
        } else {
            detailCountLabel.textColor = .black
        }
    }
}
