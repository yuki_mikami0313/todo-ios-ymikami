//
//  TodoCreateRequestTests.swift
//  TodoAppTests
//
//  Created by ymikami on 2020/06/26.
//  Copyright © 2020 ymikami. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoCreateRequestTests: XCTestCase {

    private let todoCreateRequest = TodoCreateRequest(title: "title", detail: nil, date: nil)
    
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testInit() {
        let request = todoCreateRequest
        
        XCTAssertEqual(request.method, .post)
        XCTAssertEqual(request.path, "/todos")
    }
    
    func testResponse() {
        var todoCreateReponse: CommonResponse?
        
        stub(condition: isHost("todo-api-ymikami.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }
        
        let expectation = self.expectation(description: "Create Todo Request")
        
        APIClient().call(
            request: todoCreateRequest,
            success: { response in
                todoCreateReponse = response
                expectation.fulfill()
        },
            failure: { _ in
                return
        })
        
        // 成功時のレスポンステスト
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoCreateReponse)
            XCTAssertEqual(todoCreateReponse?.errorCode, 0)
            XCTAssertEqual(todoCreateReponse?.errorMessage, "")
        }
    }
}
