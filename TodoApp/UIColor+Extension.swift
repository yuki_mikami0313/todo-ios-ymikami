//
//  UIColor+Extension.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/08.
//  Copyright © 2020 ymikami. All rights reserved.
//
import UIKit

extension UIColor {
    class var darkblue: UIColor {
        return UIColor(named: "darkblue")!
    }
    class var silver: UIColor {
        return UIColor(named: "silver")!
    }
}
