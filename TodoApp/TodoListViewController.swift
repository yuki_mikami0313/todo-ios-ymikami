//
//  TodoListViewController.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/02.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit
import Alamofire
import MaterialComponents.MaterialSnackbar

final class TodoListViewController: UIViewController {
    private var composeBarButtonItem: UIBarButtonItem!
    private var isDeleteMode = false
    private var todos: [Todo] = []

    @IBOutlet private weak var todoTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationItem()
        fetchTodoList()
    }

    private func fetchTodoList() {
        APIClient().call(
            request: TodosGetRequest(),
            success: { [weak self] result in
                self?.todos = result.todos
                self?.todoTableView.reloadData()
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func deleteTodo(id: Int) {
        APIClient().call(
            request: TodoDeleteRequest(id: id),
            success: { [weak self] _ in
                self?.fetchTodoList()
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }
    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
        let trashBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(trashBarButtonItemTapped(_:)))
        composeBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(composeBarButtonTapped(_:)))
        navigationItem.rightBarButtonItems = [trashBarButtonItem, composeBarButtonItem]
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }

    @objc private func trashBarButtonItemTapped(_ sender: UIBarButtonItem) {
        isDeleteMode.toggle()
        let systemItem: UIBarButtonItem.SystemItem = isDeleteMode ? .reply : .trash
        let trashBarButtonItem = UIBarButtonItem(barButtonSystemItem: systemItem, target: self, action: #selector(trashBarButtonItemTapped(_:)))
        navigationItem.rightBarButtonItems = [trashBarButtonItem, composeBarButtonItem]
    }

    @objc private func composeBarButtonTapped(_ sender: UIBarButtonItem) {
        goToTodoEditViewController()
    }

    private func showSnackbar(text: String) {
        let message = MDCSnackbarMessage()
        message.text = text
        MDCSnackbarManager.show(message)
    }

    private func goToTodoEditViewController(todo: Todo? = nil) {
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditViewController = storyboard.instantiateInitialViewController() as? TodoEditViewController else { return }
        todoEditViewController.delegate = self
        todoEditViewController.todo = todo
        navigationController?.pushViewController(todoEditViewController, animated: true)
    }

    private func showDeleteAlert(todo: Todo) {
        let alertController = UIAlertController(title: "「\(todo.title)」を削除します。", message: "よろしいですか？", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "キャンセル", style: .cancel))
        let deleteAction = UIAlertAction(title: "OK", style: .default) { _ in
            self.deleteTodo(id: todo.id)
        }
        alertController.addAction(deleteAction)
        present(alertController, animated: true)
    }
}

extension TodoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // セルを取得する
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath)
        // セルに表示する値を設定する
        cell.textLabel?.text = todos[indexPath.row].title
        return cell
    }

}

extension TodoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let todo = todos[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        if isDeleteMode {
            showDeleteAlert(todo: todo)
        } else {
            goToTodoEditViewController(todo: todo)
        }
    }
}

extension TodoListViewController: TodoEditViewControllerDelegate {
    func todoEditViewControllerDidTodoEdit(_ todoEditViewController: TodoEditViewController, message: String) {
        navigationController?.popViewController(animated: true)
        showSnackbar(text: message)
        fetchTodoList()
    }
}
