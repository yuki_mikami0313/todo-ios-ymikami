//
//  BaseResponse.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/18.
//  Copyright © 2020 ymikami. All rights reserved.
//

import Foundation

protocol BaseResponse: Codable {
    var errorCode: Int { get }
    var errorMessage: String { get }
}
