//
//  TestRequestProtocol.swift
//  TodoAppTests
//
//  Created by ymikami on 2020/06/29.
//  Copyright © 2020 ymikami. All rights reserved.
//

import Alamofire
@testable import TodoApp

class TestRequestProtocol: RequestProtocol {
    typealias Response = TodosGetResponse

    var path: String {
        return "/todos"
    }
    var method: HTTPMethod {
        return .get
    }
    var parameters: Parameters? {
        return nil
    }
    var baseUrl: String {
        return "https://todo-api-ymikami.herokuapp.com"
    }
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    var headers: HTTPHeaders? {
        return ["Content-Type": "application/json"]
    }
}
