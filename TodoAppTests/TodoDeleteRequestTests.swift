//
//  TodoDeleteRequestTests.swift
//  TodoAppTests
//
//  Created by ymikami on 2020/06/25.
//  Copyright © 2020 ymikami. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoDeleteRequestTests: XCTestCase {
    
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testInit() {
        let request = TodoDeleteRequest(id: 1)
        
        XCTAssertEqual(request.method, .delete)
        XCTAssertEqual(request.path, "/todos/1")
    }
    
    func testResponse() {
        var todoDeleteReponse: CommonResponse?
        
        stub(condition: isHost("todo-api-ymikami.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }
        
        let expectation = self.expectation(description: "Delete Todo Request")
        
        APIClient().call(
            request: TodoDeleteRequest(id: 1),
            success: { response in
                todoDeleteReponse = response
                expectation.fulfill()
        },
            failure: { _ in
                return
        })
        
        // 成功時のレスポンステスト
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoDeleteReponse)
            XCTAssertEqual(todoDeleteReponse?.errorCode, 0)
            XCTAssertEqual(todoDeleteReponse?.errorMessage, "")
        }
    }
}
