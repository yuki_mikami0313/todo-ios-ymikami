//
//  CommonResponse.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/18.
//  Copyright © 2020 ymikami. All rights reserved.
//

import Foundation

struct CommonResponse: BaseResponse {
    var errorCode: Int
    var errorMessage: String
}
