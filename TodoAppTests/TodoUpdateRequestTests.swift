//
//  TodoUpdateRequestTests.swift
//  TodoAppTests
//
//  Created by ymikami on 2020/06/26.
//  Copyright © 2020 ymikami. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoUpdateRequestTests: XCTestCase {

    private let todo = Todo(id: 1, title: "aa", detail: "detail", date: nil)

    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodoUpdateRequest(todo: todo)

        XCTAssertEqual(request.method, .put)
        XCTAssertEqual(request.path, "/todos/1")
    }

    func testResponse() {
        var todoUpdateReponse: CommonResponse?

        stub(condition: isHost("todo-api-ymikami.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Update Todo Request")

        APIClient().call(
            request: TodoUpdateRequest(todo: todo),
            success: { response in
                todoUpdateReponse = response
                expectation.fulfill()
        },
            failure: { _ in
                return
        })

        // 成功時のレスポンステスト
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoUpdateReponse)
            XCTAssertEqual(todoUpdateReponse?.errorCode, 0)
            XCTAssertEqual(todoUpdateReponse?.errorMessage, "")
        }
    }
}
