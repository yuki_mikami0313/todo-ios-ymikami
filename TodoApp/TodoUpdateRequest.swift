//
//  TodoUpdateRequest.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/22.
//  Copyright © 2020 ymikami. All rights reserved.
//

import Foundation
import Alamofire

struct TodoUpdateRequest: RequestProtocol {
    typealias Response = CommonResponse

    let todo: Todo

    var path: String {
        return "/todos/\(todo.id)"
    }

    var method: HTTPMethod {
        return .put
    }

    var parameters: Parameters? {
        var parameters = ["title": todo.title]
        if let detail = todo.detail {
            parameters["detail"] = detail
        }
        if let date = todo.date {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            parameters["date"] = formatter.string(from: date)
        }
        return parameters
    }
}
