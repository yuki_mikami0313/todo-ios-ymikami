//
//  TodosGetResponse.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/16.
//  Copyright © 2020 ymikami. All rights reserved.
//

import Foundation

struct TodosGetResponse: BaseResponse {
    var todos: [Todo]
    var errorCode: Int
    var errorMessage: String
}
