//
//  Todo.swift
//  TodoApp
//
//  Created by ymikami on 2020/06/11.
//  Copyright © 2020 ymikami. All rights reserved.
//

import Foundation

struct Todo: Codable {
    let id: Int
    var title: String
    var detail: String?
    var date: Date?
}
